# BUILD
FROM golang:1.16-alpine3.13 AS builder
WORKDIR /app
COPY ./src .
RUN go build -o main main.go

# RUN
FROM scratch
WORKDIR /app
COPY --from=builder /app/main .
CMD ["/app/main"]
